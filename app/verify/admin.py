from django.contrib import admin

from . import models


@admin.register(models.File)
class FileAdmin(admin.ModelAdmin):
    # TODO: добавить отображение количества связанных првоерок и их кодов ответов
    model = models.File
    list_display = (
        'modified', 'created',
        'name',
        'upload_file',

    )
    ordering = ('modified',)
    actions = ('check_emails',)

    def check_emails(self, request, queryset):
        for obj in queryset:
            obj.check_emails()


@admin.register(models.Verify)
class VerifyAdmin(admin.ModelAdmin):
    model = models.Verify
    list_display = (
        'file', 'email', 'result_code',
        'modified', 'created'
    )
