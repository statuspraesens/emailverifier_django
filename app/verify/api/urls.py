from django.urls import re_path, path

from . import views

app_name = 'verify'

urlpatterns = [
    path('<str:email>', views.VerifyAPIView.as_view(), name='verify')
]
