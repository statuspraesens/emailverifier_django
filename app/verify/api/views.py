from rest_framework.views import APIView
from rest_framework.response import Response

from verify.clients import MillionVerifierClient
from app.verify.models import Verify


class VerifyAPIView(APIView):

    def get(self):
        email = self.kwargs['email']
        client = MillionVerifierClient()
        code_response = client.check_email(email=email)
        Verify.objects.create(
            email=email,
            file=None,
            result_code=code_response
        )
        return Response(data={'resultcode': code_response})
