from urllib.parse import urlencode

from django.conf import settings

import requests


class MillionVerifierClient:

    def __init__(self):
        self.url = f'{settings.PROXY_DOMAIN}t/{settings.PROXY_TOKEN}/https/api.millionverifier.com/api/v3/?api=fz2vnThkUvd3hByjPFQOh5Kl4'

    def check_email(self, email) -> int:
        """
        Посылает запрос на проверку email в api.millionverifier.com через наш прокси сервер
        :param email: проверяемый email
        :return: код ответа
        """
        response = requests.get(url=self.url, params={'email': email})
        j = response.json()
        result_code = j['resultcode']
        return result_code
