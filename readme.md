Сервис проверки email

есть два способа проверить email:

через апи проверить один email
```
curl -X GET \
https://emailverifier.praesens.group/api/v1/verify/<email>
```

Второй, с помощью .xlsx таблицы
в таблице должна быть колонка email, в которой и находятся проверяемые email

загрузить файл можно в http://127.0.0.1:8033/admin/verify/file/
После загрузки выбрать таблицу и выполнить действие "Check emails"